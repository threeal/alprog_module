# Chapter 4, Function and Parameter

## 4.1 Function Definition

Function is part of a program that used to do some task where the position is separated from the program that uses it.
Based on the input and the output, a function could be separated into 4 types.
Input in a function is a function that requires some parameter, while output in a function is a function that returns some value.
1. Function without an input and an output, Example: `custom_function();`
2. Function with an input but without an output, Example: `printf("Hello");`
3. Function without an input but witn an output, Example: `time();`
4. Function with both an input and and output, Example: `atoi(str);`

## 3.2 Parameterless Function

This function is a function that does not have an input parameter, but this function will still do the task.
Example:
``` cpp
void function_name()
{
  /* statement */
}
```

Full usage example of parameterless function:
``` cpp
#include <stdio.h>

char name[50], student_id[20];

void input_data()
{
  // first function
  printf("Name : ");
  scanf("%s", &name);
  printf("Student ID : ");
  scanf("%s", &student_id);
}

void string_print()
{
  printf("This function will output a string\n");
  printf("please input the data\n);
  input_data();
}

int main(int argc, char **argv)
{
  string_print();
  return 0;
}
```

## Function with Parameter

This function is a function that has some parameters with some task.
please notice that each parameter is independent and will not be changed even if it's changed inside the function.
Example:
``` cpp
void function_name(data_type argument1, data_type argument2, ...)
{
  /* statement */
}
```

If the change of an argument value in a function also need to change the value of a variable, a star symbol (*) will be needed to indicate if the value in the argument will be passed by reference.
Example:
``` cpp
void function_name(data_type *argument1, data_type *argument2, ...)
{
  /* statement */
}
```


Full usage example of function with parameter:
``` cpp
#include <stdio.h>

void string_print(char str1[], char str2[])
{
  printf("Student name is %s\n", str1);
  printf("Student ID is %s\n", str2);
}

void calculate(int a, int b)
{
  printf("Result of %d + %d is %d\n", a, b, a + b);
}

int main(int agrc, char **argv)
{
  char name[50], student_id[20];
  int a, int b;
  printf("Name : "); scanf("%s", &name);
  printf("Student ID : "); scanf("%s", &student_id);
  string_print(name, student_id);

  a = 10; b = 12;
  calculate(a, b);

  return 0;
}
```

## 4.4 Function with Return Value

Function with return value is a function that returns some value to the function caller.
Example:
``` cpp
data_type function_name(data_type argument1, data_type argument2, ...)
{
  /* statement */
  return some_value;
}
```

Full usage example of function with return value:
``` cpp
#include <stdio.h>

int calculate(int a, int b)
{
  return a + b;
}

int main(int argc, char **argv)
{
  int a = 10, b = 12;
  printf("Result of %d + %d is %d\n", a, b, calculate(a, b));

  return 0;
}
```

## 4.5 Recursive Function

Recursive function is a function that calls itself multiple times.
Because the process is done repeatedly, there must be a condition to end the process.
Full usage example of recursive function:

``` cpp
#include <stdio.h>

void factorial(int n)
{
  if (n <= 1)
    return 1;
  else
    return n * factorial(n - 1);
}

int main(int argc, char **argv)
{
  int n;
  printf("Input an integer : "; scanf("%d", &n);
  printf("Factorial of %d is %d\n", n, factorial(n));

  return 0;
}
```