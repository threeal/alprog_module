# Chapter 3, Program Control

## 3.1 If Statement

In an if statement model, an action will be done if the submitted requirement is true.

> `Syarat = Requirement`
> `Benar = True`
> `Salah = False`
> `Aksi1 = Action1`

Image 3.1: If statement in the flowchart

Example of if statement:

``` cpp
int a = 5;
if (a > 0) {
  printf("a is positive");
}
```

An if statement could be developed into an if else statement.

> `Syarat = Requirement`
> `Benar = True`
> `Salah = False`
> `Aksi1 = Action1`
> `Aksi2 = Action2`

Image 3.2: If else statement in the flowchart

In this statement model, Action1 will be done if the requirement is true and when it is false then Action2 will be done.
Example of if else statement:

``` cpp
int a = 5;
if (a > 0) {
  printf("a is positive");
} else {
  printf("a is not positive");
}
```

An if else statement could be created as a nested if else depends on the requirement in the program.

> `Syarat = Requirement`
> `Benar = True`
> `Salah = False`
> `Aksi1 = Action1`
> `Aksi2 = Action2`
> `AksiN = ActionN`

Image 3.3: Nested if else statement in the flowchart

Example of nested if else statement:
``` cpp
int a = 5;
if (a > 0) {
  printf("a is positive");
} else if (a < 0) {
  printf("a is negative");
} else {
  printf("a is zero");
}
```

In this statement model, Action1 will be done if Requirement1 is true and if it's wrong, Requirement2 will be checked.
If Requirement2 is true then Action2 will be done, else, Action3 will be done.

## 3.2 For Statement

For statement loop in C language allow a code to be run multiple time.
A for loop requires an iterator variable, commonly it uses i notation, but it could also use another variable name.
This loop method is used to:
- Initialize an iterator variable with a predetermined value.
- Check if the iterator variable value has reached some value.
- Changes the value of the iterator variable.

Example of a for loop statement to loop a program code 10 times:
``` cpp
int i;
for (i = 0; i < 10; i++) {
  printf("%d\n", i);
}
```

That code will print a number from 0 to 9 (repeat a printf() command 10 times).
A for loop could be used to do iteration in an array.
Example of array sum calculation:
``` cpp
int array[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
int sum = 0;
int i;
for (i = 0; i < 10; i++) {
  sum += array[i];
}
/* sum will contains a[0] + a[1] + ... + a[9] */
printf("Sum of the array is %d\n, sum);
```

## 3.3 While Statement

A while loop works similarly with a for loop, but with less functions.
A while loop will always execute the program inside it as long as the requirement is fulfilled.
Example of While statement to run a program code 10 times:
``` cpp
int n = 0;
while (n < 10) {
  n++;
}
```

A while loop will always execute a code if the requirement in the while statement always true.
``` cpp
while (1) {
  /* do something */
}
```

## 3.4 Loop Directives

There are 2 ways to control the flow of a loop in C language, using a break directive and a continue directive.
In the following code, the break directive will be used to stop a loop after 10 iterations, even if the required condition still fulfilled.
``` cpp
int n = 0;
while (1) {
  n++;
  if (n == 10) {
    break;
  }
}
```

In the following code, the continue directive causes the printf() command to be skipped, so that only even
numbers are printed out:
``` cpp
int n = 0;
while (n < 10) {
  n++;
  if (n % 2 == 1) {
    continue;
  }

  printf("%d number is even.\n", n);
}
```