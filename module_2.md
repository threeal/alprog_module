# Chapter 2, Data Type and Variable

## 2.1 Data Type

C language has some data type, here are some data types in C language:

- Integer - Contains a positive or negative number.
  Could be declared with char, short, int, long, or long long.
- Unsigned Integer - Contains only a positive number.
  Could be declared with unsigned char, unsigned short, unsigned int, unsigned long, or unsigned long long.
- Floating Point Numbers - Contains a real number (number that contains a decimal).
  Could be declared with float or double.
- Structure - Will be explained later in the structure chapter.

Using a different data type will determine the maximum number that may be stored.
Char could only contain a number from -128 to 127, while long could contain a number from -2,147,483,648 to 2,147,483,647.
Please note that C language doesn't have a boolean data type.
Generally, boolean could be defined using this notation:

``` c
#define BOOL char
#define FALSE 0
#define TRUE 1
```

C language uses an array of char to define a string, this will be explained later in the string chapter.

## 2.2 Variable

Variable is used to store a value that can be changed.
Number variable usually use int data type.
Int in most computers nowadays is a 32-bit or 8-byte integer.
Thus, this variable can contain numbers from -2,147,483,648 to 2,147,483,647 (size of int is equal to size of long).
To declare foo and bar variables, we can use this syntax:

``` c
int foo;
int bar = 1;
```

Foo variable in the above code may be used directly (added, substracted, printed).
But we haven't done the initialization, so we couldn't know the content of the foo variable.
Bar variable in that code contains 1 value.

After declaring a variable, we could do mathematics operation.
With assumption that a, b, c, d, and e variable have been declared, we could do mathematics operation and save it in a variable with this notation:

``` c
int a = 0, b = 1, c = 2, d = 3, e = 4;
a = b - c + d * e;
printf("%d", a);/* will print 1 - 2 + 3 * 4 = 11 */
```

## 2.3 Array

Array is a special variable that could store more than one variable with the same data type. Declaration of array could use the following syntax:

``` c
/* declaration of an array that contain 10 integers */
int numbers[10];
```

To access a number in an array, we could use the same syntax.
Please notice that an array in C language starts from 0.
Thus if an array is declared with a size of 10, the element in an array is start from 0 to 9.

``` c
int numbers[10];
/* populate the array */
numbers[0] = 10;
numbers[1] = 20;
numbers[2] = 30;
numbers[3] = 40;
numbers[4] = 50;
numbers[5] = 60;
numbers[6] = 70;
/* print the 7th element from the array, so we use index 6 */
printf("The 7th elemnt of the array is %d", numbers[6]);
```

An array can only consist of just 1 data type, because array actually implement a sequence of value in the computer memory.
Because of that, accessing an array randomly is efficient (not decreasing program performance).

## 2.4 String

### 2.4.1 String Definition

String in C language actually is an array of characters.
Sometimes, pointer is used to define a string in C language.
Even if the pointer will be explained later in the next chapter, in this chapter we will use a pointer to point an array of characters that represent a string using the following syntax:

``` c
char *name = "Bung Karno";
```

This method only creates a string that could only be used to read, nonmodifiable.
If we want to create a string that could be modified, the declaration of the string could use an array of characters like the following syntax:

``` c
char name[] = "Bung Karno";
```

That syntax is different from the previous syntax, because string in that syntax is allocated statically, so it could be modified.
That syntax is different from the previous syntax, because string in that syntax is allocated statically, so it could be modified.
The empty square bracket declares that the size of the array will be automatically calculated by the compiler.
In this case, explicit array size declaration will also result in the same array type.
Note that the written size is added by one.

``` c
char name[] = "Bung Karno";
/* is the same as */
char name[11] = "Bung Karno";
```

Even if the string length is longer, there is a terminate character (special character with values of 0) that indicates the end of the string.

### 2.4.2 String Formating

We could use the printf function to format a string with another string like the following example:

``` c
char *name = "Bung Karno";
int age = 27;

/* print 'Bung Karno is 27 years old.' */
printf("%s is %d years old.\n", name, age);
```

Note that when we print a string, we could add a new line character (\n) that indicate to move to the new line.

### 2.4.3 String Comparison

strncmp function is used to do a comparison between 2 strings.
This function will return 0 when both strings are identical and return nonzero when both strings are different.
The arguments of strncmp function are two strings that will be compared and the maximum length of the string that will be compared, like the following example:

``` c
char *name = "Bung";

if (strncmp(name, "Bung", 4) == 0) {
  printf("Hello, Bung!\n");
} else {
  printf("You are not Bung Karno!\n");
}
```