# Chapter 1, Introduction to IDE

In this module, we will be introduced to CodeLite, an editor to create a program in C language, and how to use it.

## 1.1 Integrated Development Environment (IDE)

In C language, there is a lot of IDE that we could use, but in this practicum, we will use the CodeLite IDE.
CodeLite is a derivation of CodeBlocks.
CodeLite is a program code editor, an interactive help, and debugger that could be used to do the development of program using C language.
As shown in image 1.5, CodeLite layout is divided by 3, Editor, Workspace, and Output.

> Image 1.1: The display of CodeLite IDE

The CodeLite IDE consists of 3 main part, Editor View, Workspace View, and Output View:

1. Editor View\
Editor in CodeLite that is used to write the program code.
The text editor already supports auto-completion by using the ctrl+space combination button.
The auto-completion will suggest every possible C keywords that could be used in the program code.
You will work a lot using this text editor here.

2. Workspace View\
File explorer of CodeLite project.
This part is used to view and choose files that want to be modified.

3. Output View\
Consist of many tabs that used to show output from the process that exists in the CodeLite.
The output could either be the result of build project or run project.

### 1.1.1 GCC Project

In this practicum, the compiler that will be used is GCC.
The steps of project creation is first, choose a new project from the main menu.

> Image 1.2: New Project menu

Choose Simple Executable (GCC) as a project type that will be built.

> Image 1.3: The GCC project

Set the project name and the location to save the project

> Image 1.4: Project location

Choose the compiler that will be used.
In this practicum, we will use MinGW Compiler (CodeLite).

> Image 1.5: MinGW compiler

Then press the Finish button to start writing the program code.

### 1.1.2 Practicum CodeLite

In this practicum, a CodeLite plugin has been developed to help you download the provided program code template and to check the correctness of the program that you have created.
This plugin will do a connection to the predetermined server to save your progress during the practicum.
This plugin consists of 4 menus:

- This menu is used to login to the server.
Use your student id and your class to login.
If you fail to login, please call the assistant.

- This menu is used to download the program template code in every practicum module.
For each assignment in the practicum module, there is already a template that can make it easy for you.
Use that template!

- This menu is used to check the correctness of the program that you have been created.
For each assignment in this practicum module, there are already test parameters that will be used to test the correctness of your program.
Progress of the practicum can be seen in the front display that will be shown by the assistant.

- This menu is used to set the address of the server and the script location in the server.
Fill both value as instructed by the assistant.

## 1.2 Program in C Language

C programming language is a general-purpose language, where the syntax is close to how it correlated to how the machine work.
The knowledge of how memory works is a fundamental aspect in C programming language.
C is a language that commonly used to develop many programs like Windows, Python Interpreter, Git, and many more others.
C is compiled language, in other words, to run a program from C language, a compiler (like GCC or Visual Studio) will read the code that we write, processed it, and create an executable file from it.

## 1.2.1 First Program

Every program in C language required a library that gives an ability to execute some functions.
For example, the basic function that commonly used is printf, that give s ability to print something to the output.
This function is defined in the stdio.h header file.
To use the printf function in the program, we must add an include declaration like the following:

``` c
#include <stdio.h>
```

The second part of the code is your code itself.
First code that will be run will always start from the main function.

``` c
int main () {
  // code start from here
}
```

the int word indicates that the main function will return an integer value.
In this main function case, the value shows whether the program ran correctly or not.
If we want to tell that the program ran correctly, return the program with 0 value.
Value other than 0 shows that the program is failed.
In this practicum, we will return the program with 0 value to indicate that the program ran correctly.

``` c
return 0;
```

Notice that every statement in C language always ended with semicolon (;).
This is used by the compiler to distinguish between one statement with another.
Also notice in the above code, there is double forward slash characters.
These characters are used to create a comment in the code.
Comment is a part of the code that will not be processed by the compiler.
There is 2 way to create a comment in C language, by using double forward slash and combination of forward slash and star.

``` c
// it will comment on this statement until the end of the line.
// the next comment must repeat the same symbol.

/* it will comment on this statement until the next finishing symbol.
this kind of comment is used on statement with more than one line. */
```